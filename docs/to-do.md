# The to-do list

_Not necessarily in this order ..._

## Features

### Confluence API

The low-level crud ops for Atlassian.

- [x] Create a new page
- [x] Fetch page by id
- [x] Fetch page properties by key
- [x] Update page properties by key

### App API

The api routes in the Remix app.

#### Create a page in Confluence

- [x] **`createPage`** – Creates a new session
- [x] Creates a new wiki page from a specified date string
- [x] Sets some initial page properties for nice formatting 💅
- [x] Send slack notification when a page is created
- [ ] Updates the big table on the parent page with latest date + link to wiki page

#### Send reminders in Slack

- [x] Send slack reminder at a scheduled period before Demo Day
  - [ ] Include Likes count _(social proof)_
  - [ ] Use scheduled messages in the Slack API?

### Near Future

- [ ] Deploy to production 🚀
- [ ] Implement some auth n' auth 🔐
- [ ] Create a UI (probably needs a database) 💻 🧳
  - [ ] Deploy some sort of db _(whatever's popular at heb for this type o' thing)_
  - [ ] Manage past & future demo day sessions via UI
  - [ ] Allow sign-ups via UI
- [ ] Allow sign-ups via Slack bot 🤖
- [ ] Auto-create the next session immediately after the current one ends 📆
  - [ ] Could we have our UI or bot allow us to "bump" presenters to a future session, or waiting list? _(for example, if we run out of time, or a presenter has technical issues)_
- [ ] Add Sentry 💂
- [ ] So much more Slack integration ... we're just using webhooks so far

## Code quality

- [ ] TS-ify it 💪 _(There's already lots of jsdoc comments in the code to help with that)_
- [ ] Improve error handling everywhere 🚨
  - [ ] Especially when calling the Confluence API
- [ ] Some log level control would be nice 🪵
- [ ] Resolve a million typescript errors 🤦‍♂️
- [ ] Test everything 🧪
