# How I made Demo Buddy

## APIs

### Atlassian Confluence Cloud API

#### Download the Postman collection

_This was super helpful!_

- Download the [Confluence Cloud REST API v2](https://developer.atlassian.com/cloud/confluence/rest/v2/intro/#about) collection, and import it into Postman.

  ![The Confluence Cloud REST API v2](img/download-postman-collection.png "The Confluence Cloud REST API v2")

  ![The Confluence Cloud REST API v2](img/The-Confluence-Cloud-REST-API-v2.png "The Confluence Cloud REST API v2")

#### Creating a new Demo Day session page

1. [POST Create page](https://developer.atlassian.com/cloud/confluence/rest/v2/api-group-page/#api-pages-post)
2. [PUT Update content property for page by id](https://developer.atlassian.com/cloud/confluence/rest/v2/api-group-content-properties/#api-pages-page-id-properties-property-id-put)

### Slack API

### To-do

See the [to-do list](./to-do.md)

## Ergonomics

_(aka the "developer experience")_

### VS Code extensions

- [ToDo Tree](https://marketplace.visualstudio.com/items?itemName=Gruntfuggly.todo-tree)
- [Better Comments](https://marketplace.visualstudio.com/items?itemName=aaron-bond.better-comments)

### Working locally

- I used TypeScript for all the benefits in VS Code, but I didn't run my build through `tsc`. I can live with a bunch of red squigglies, but I can't let types or a compiler slow my roll. This was critical to working faster.
- When types are too heavy _(a simple function with some primitive arg types)_, just use JSDoc. It's built into VS Code, and gives you the benefit of documentation until you do, or don't, add types later.
