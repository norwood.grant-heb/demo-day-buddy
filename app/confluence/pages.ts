import { CONFLUENCE_BASE_URL } from "~/config/config";
import {
  logInfo,
  logSuccess,
  logApiErrors,
  logError,
  logApiJsonResult,
  logApiResponse,
  logErrorMessage,
} from "~/utils/logging";
import { getAuthorizationHeader } from "~/utils/api";
// import { deepCopy } from "~/utils/utils";

/** ---- Types ---- */

/**
 * A simpler copy of the ConfluenceCreatePageResponse for general use.
 *
 * TODO: Add the other complex types commented out below
 */
export interface ConfluencePage {
  id?: string;
  // status: ContentStatus;
  title?: string;
  spaceId?: string;
  parentId?: string;
  // parentType: ParentContentType;
  position?: number;
  authorId?: string;
  ownerId?: string;
  lastOwnerId?: string;
  createdAt?: string;
  // version: Version;
  // body: BodySingle;
  labels?: object;
  properties?: object;
  operations?: object;
  likes?: object;
  versions?: object;
  isFavoritedByCurrentUser?: boolean;
  _links?: AbstractPageLinks;
}

export interface AbstractPageLinks {
  editui?: string;
  webui?: string;
  tinyui?: string;
  base?: string;
}

/**
 * See https://developer.atlassian.com/cloud/confluence/rest/v2/api-group-page/#api-pages-post-response
 *
 * TODO: Add the other complex types commented out below
 */
export interface ConfluenceCreatePageResponse {
  id: string;
  // status: ContentStatus;
  title: string;
  spaceId: string;
  parentId: string;
  // parentType: ParentContentType;
  position: number;
  authorId: string;
  ownerId: string;
  lastOwnerId: string;
  createdAt: string;
  // version: Version;
  // body: BodySingle;
  labels: object;
  properties: object;
  operations: object;
  likes: object;
  versions: object;
  isFavoritedByCurrentUser: boolean;
  // _links: AbstractPageLinks;
}

export interface CreatePageRequestBody {
  spaceId: string;
  status: string;
  title: string;
  parentId: string;
  body: {
    representation: string;
    value: string;
  };
}

export interface Track {
  title: string;
  url: string;
}

export interface CreateNewSessionPageRequestArgs {
  date: string;
  customTitle: string;
  customMessage: string;
  featuredTracks: Track[];
}

/** ---- Functions ---- */

/**
 * Get a page by id.
 */
export async function getPageById(pageId: string): Promise<ConfluencePage> {
  try {
    logInfo(`Calling the api to get page "${pageId}" ...`);

    const response = await fetch(`${CONFLUENCE_BASE_URL}/pages/${pageId}?body-format=storage`, {
      method: "GET",
      headers: {
        Authorization: getAuthorizationHeader(),
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    });

    // DEBUG
    logApiResponse(response);

    const result = await response.json();

    // DEBUG
    logApiJsonResult(result);

    if (result.errors && result.errors > 0) {
      logApiErrors(result.errors);

      return result;
    }

    const newPage = { ...result } as ConfluencePage;

    if (result.id) {
      logSuccess(`Called the api to get page with id "${result.id}"\n`);
    } else {
      logErrorMessage(`Failed to get page with id "${pageId}"\n${JSON.stringify(result, null, 2)}`);

      // TODO: Return errors, don't just log 'em
    }

    return newPage;
  } catch (error) {
    // TODO: Handle the error better
    logErrorMessage(`Failed to get page`);
    logError(error!);

    return { errors: [error] };
  }
}

/**
 * Get a page by id.
 */
export async function getPageLikeCount(pageId: string): Promise<number> {
  try {
    logInfo(`Calling the api to get likes for page "${pageId}" ...`);

    const response = await fetch(`${CONFLUENCE_BASE_URL}/pages/${pageId}/likes/count`, {
      method: "GET",
      headers: {
        Authorization: getAuthorizationHeader(),
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    });

    // DEBUG
    logApiResponse(response);

    const result = await response.json();

    // DEBUG
    logApiJsonResult(result);

    if (result.errors && result.errors > 0) {
      logApiErrors(result.errors);

      return result;
    }

    const count = result.count;

    if (count >= 0) {
      logSuccess(`Called the api to get likes for page with id "${result.id}"\n`);
    } else {
      logErrorMessage(`Failed to get likes for page with id "${pageId}"\n${JSON.stringify(result, null, 2)}`);

      // TODO: Return errors, don't just log 'em
    }

    return count;
  } catch (error) {
    // TODO: Handle the error better
    logErrorMessage(`Failed to get page`);
    logError(error!);

    return { errors: [error] };
  }
}

/**
 * Create a new page in Confluence, and set some initial properties.
 *
 * @param date    string    The short date string, e.g. "2024-05-15"
 */
export async function createPage(date: string, data: CreatePageRequestBody): Promise<ConfluenceCreatePageResponse> {
  try {
    logInfo(`Calling the api to create a page called "${data.title}" ...`);

    const response = await fetch(`${CONFLUENCE_BASE_URL}/pages`, {
      method: "POST",
      headers: {
        Authorization: getAuthorizationHeader(),
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });

    // DEBUG
    logApiResponse(response);

    const result = await response.json();

    // DEBUG
    logApiJsonResult(result);

    if (result.errors && result.errors > 0) {
      logApiErrors(result.errors);

      return result;
    }

    const newPage = { ...result } as ConfluencePage;

    if (result.id) {
      logSuccess(`Called the api to create page with id "${result.id}"\n`);
    } else {
      logErrorMessage(`Failed to create page (result.id was null or undefined)\n${JSON.stringify(result, null, 2)}`);

      // TODO: Return errors, don't just log 'em
    }

    return newPage;
  } catch (error) {
    // TODO: Handle the error better
    logErrorMessage(`Failed to create page`);
    logError(error!);

    return { errors: [error] };
  }
}
