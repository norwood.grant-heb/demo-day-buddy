import { CONFLUENCE_BASE_URL } from "~/config/config";
import {
  logInfo,
  logSuccess,
  /* logApiErrors, */ logError,
  logApiJsonResult,
  logApiResponse,
  logApiErrors,
} from "~/utils/logging";
import { getAuthorizationHeader } from "~/utils/api";

/** ---- Types ---- */
export interface GetPagePropertyArgs {
  pageId: string;
}

export interface PageProperty {
  value: string;
  key: string;
  id: string;
  version: {
    number: number;
    message?: string;
    minorEdit?: boolean;
    authorId?: string;
    createdAt?: string;
  };
}

export interface UpdatePagePropertyArgs {
  pageId: string;
  propertyId: string;
  key: string;
  value: string;

  /**
   * Latest version + 1
   */
  versionNumber: number;
}

/** ---- Functions ---- */

export function findPagePropertyByKey(key: string, properties: PageProperty[]) {
  const property: PageProperty = properties.find((p) => p.key === key) as PageProperty;

  return property;
}

/**
 * Updates a content property for a page.
 *
 * TODO: Should we return an unknown type?
 *
 * @param date
 * @returns
 */
export async function getPageProperties({ pageId }: GetPagePropertyArgs): Promise<PageProperty[]> {
  try {
    logInfo(`Calling the api to get page properties ...`);

    const response = await fetch(`${CONFLUENCE_BASE_URL}/pages/${pageId}/properties`, {
      method: "GET",
      headers: {
        Authorization: getAuthorizationHeader(),
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    });

    logApiResponse(response);

    if (response.status !== 200) {
      logError(result);

      // TODO: This type doesn't match
      return response;
    } else {
      const result = await response.json();

      logSuccess(`Called the api to get page properties\n`);
      logApiJsonResult(result);

      return result.results;
    }
  } catch (error) {
    // TODO: Handle the error better
    // DEBUG: don't forget to remove me!
    logError(error!);

    // TODO: This type doesn't match
    return error;
  }
}

/**
 * Updates a content property for a page.
 *
 * @param date
 * @returns
 */
export async function updatePageProperty({ pageId, propertyId, key, value, versionNumber }: UpdatePagePropertyArgs) {
  const data = {
    id: propertyId,
    key,
    value,
    version: {
      number: versionNumber,
    },
  };

  try {
    logInfo(`Calling the api to update page property "${data.key}" with value ${data.value} ...`);

    const response = await fetch(`${CONFLUENCE_BASE_URL}/pages/${pageId}/properties/${propertyId}`, {
      method: "PUT",
      headers: {
        Authorization: getAuthorizationHeader(),
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });

    logApiResponse(response);

    const result = await response.json();

    // logApiJsonResult(result);

    if (result.errors) {
      logApiErrors(result.errors);
      // logApiJsonResult(result);
    } else {
      logSuccess(`Called the api to update page ${pageId} property "${key}" with value "${value}"\n`);
    }

    return result;
  } catch (error) {
    // TODO: Handle the error better
    // DEBUG: don't forget to remove me!
    logError(error);

    return { errors: [error] };
  }
}
