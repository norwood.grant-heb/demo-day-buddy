import { sendErrorMessage } from "~/slack/slack";

/**
 * ---- types ----
 */

export type AppError = {
  message: string;
};

/**
 * ---- functions ----
 */

/**
 * Send the error to the console, as well as to Slack.
 *
 * TODO: Send error to Sentry
 */
export function handleError(e, sendToSlack = false) {
  console.log(`\n🚨 Error:\n${JSON.stringify(e.stack || e, null, 2)}\n`);

  // Send error to Slack
  // TODO: Improve this error message
  if (sendToSlack) {
    sendErrorMessage(e.stack || e);
  }
}

/**
 * Send the error to the console, as well as to Slack.
 *
 * TODO: Send error to Sentry
 */
export function handleSlackWebhookError(e, sendToSlack = false) {
  console.log(`\n🚨 Error:  ${e.original.message}  (${e.code})\n\n${e.stack || e}\n`);

  // Send error to Slack
  if (sendToSlack) {
    sendErrorMessage(e.stack || e);
  }
}

/**
 * Thanks for the util functions, Kent!
 * (https://kentcdodds.com/blog/get-a-catch-block-error-message-with-typescript)
 */
export function isDemoDayError(error: unknown): error is AppError {
  return (
    typeof error === "object" &&
    error !== null &&
    "message" in error &&
    typeof (error as Record<string, unknown>).message === "string"
  );
}

export function toDemoDayError(maybeError: unknown): AppError {
  if (isDemoDayError(maybeError)) return maybeError;

  try {
    return new Error(JSON.stringify(maybeError));
  } catch {
    // fallback in case there's an error stringifying the maybeError
    // like with circular references for example.
    return new Error(String(maybeError));
  }
}

export function getErrorMessage(error: unknown) {
  return toDemoDayError(error).message;
}
