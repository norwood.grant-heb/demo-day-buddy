import { IncomingWebhookResult } from "@slack/webhook";
import { AppError } from "./errors";

export type LogInfoArgs = string;

export type LogErrorArgs = {
  id?: string;
  code?: string;
  message?: string;
};

export type ConfluenceApiError = {
  status?: string;
  code?: string;
  title?: string;
  detail?: string;
};

export type LogApiErrorsArgs = [ConfluenceApiError];

/**
 *  Log a helpful message to the console.
 *
 * @param message     string      A helpful message
 */
export function logInfo(message: LogInfoArgs): void {
  console.log(`\n🪵  ${message}\n`);
}

/**
 *  Log a success message to the console.
 *
 * @param message     string     A helpful message
 */
export function logSuccess(message: LogInfoArgs, obj?: object): void {
  if (obj) {
    console.log(`✅  ${message}\n`, obj);
  } else {
    console.log(`✅  ${message}`);
  }
}

/**
 * TODO: Use TS function overloading signatures?
 */
export function logError(error: LogErrorArgs): void {
  console.log(`\n🚨  Error\n${JSON.stringify(error, null, 2)}\n`);
}

/**
 * TODO: Use TS function overloading signatures?
 *
 * @param message     string    The message object.
 */
export function logErrorMessage(message: string): void {
  console.log(`\n🚨  ${message}\n`);
}

/**
 * TODO: Use TS function overloading signatures?
 */
export function logAppErrors(errors: AppError[]): void {
  if (!errors) {
    // This shouldn't be null or undef
    console.log(`\n🚨  logAppErrors(errors) : errors was null or undefined\n`);
    return;
  }

  if (errors.length && errors.length > 0) {
    for (const e in errors as AppError[]) {
      console.log(`\n🚨  AppErrors\n${JSON.stringify(e, null, 2)}\n`);
    }
  } else {
    // TODO: Handle unrecognized objects
    console.log(`\n🚨  AppErrors : unrecognized object\n${JSON.stringify(errors, null, 2)}\n`);
  }
}

/**
 * TODO: Use TS function overloading signatures?
 */
export function logApiErrors(errors: LogApiErrorsArgs): void {
  if (!errors) {
    // This shouldn't be null or undef
    console.log(`\n🚨  logApiErrors(errors) : errors was null or undefined\n`);
    return;
  }

  if (errors.length && errors.length > 0) {
    for (const e in errors as LogApiErrorsArgs) {
      console.log(`\n🚨  ApiErrors\n${JSON.stringify(e, null, 2)}\n`);
    }
  } else {
    // TODO: Handle unrecognized objects
    console.log(`\n🚨  ApiErrors : unrecognized object\n${JSON.stringify(errors, null, 2)}\n`);
  }
}

/**
 * TODO: Use TS function overloading signatures?
 *
 * @param result            The api result object.
 * // @param result.blah       The blah of the api result.
 * // @param result.blah      The blah of the api result.
 */
export function logApiJsonResult(result): void {
  console.log(`\n🛜  ApiJsonResult\n${JSON.stringify(result, null, 2)}\n`);
}

/**
 * See how to handle Slack api errors at https://api.slack.com/messaging/webhooks#handling_errors
 *
 * TODO: Use TS function overloading signatures?
 *
 * @param result            The api result object.
 * // @param result.blah       The blah of the api result.
 * // @param result.blah      The blah of the api result.
 */
export function logIncomingWebhookResult(result: IncomingWebhookResult): void {
  console.log(`\n🛜  IncomingWebhookResult\n${JSON.stringify(result, null, 2)}\n`);
}

/**
 * TODO: Use TS function overloading signatures?
 *
 * @param response      Response     The api response object.
 */
export function logApiResponse(response: Response): void {
  console.log(`\n🛜  ApiResponse
    ${response.url} (${response.status} ${response.statusText})
    ${response.body}
  `);
}
