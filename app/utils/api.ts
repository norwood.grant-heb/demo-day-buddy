import { CONFLUENCE_USERNAME, CONFLUENCE_TOKEN } from "~/config/config";

export function getAuthorizationHeader() {
  return `Basic ${Buffer.from(`${CONFLUENCE_USERNAME}:${CONFLUENCE_TOKEN}`).toString("base64")}`;
}
