import { IncomingWebhook } from "@slack/webhook";

import { SLACK_WEBHOOK_URL_PRODUCTION, SLACK_WEBHOOK_URL_SANDBOX } from "~/config/config";

export const SLACK_DEFAULT_ARGS = {
  icon_emoji: ":bowtie:",
};

export const slackWebhookPde = new IncomingWebhook(SLACK_WEBHOOK_URL_PRODUCTION, SLACK_DEFAULT_ARGS);
export const slackWebhookSandbox = new IncomingWebhook(SLACK_WEBHOOK_URL_SANDBOX, SLACK_DEFAULT_ARGS);
