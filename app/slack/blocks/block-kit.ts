/**
 * Use Slack's Block Kit (https://api.slack.com/block-kit)
 */

/**
 * ---- types ----
 */

export type BlockTypes = "context" | "divider" | "header" | "section";

export interface BlockText {
  type: string;
  text: string;
}

export interface Block {
  type: BlockTypes;
}

export interface DividerBlock extends Block {}

export interface EmptyLineBlock extends Block {
  text?: BlockText;
}

export interface HeaderBlock extends Block {
  text?: { type: string; text: string };
}

export interface ContextBlock extends Block {
  text?: { type: string; text: string };
}

/**
 * ---- constants ----
 */

export const dividerBlock: DividerBlock = {
  type: "divider",
};

export const sectionBlock: EmptyLineBlock = {
  type: "section",
  text: {
    type: "plain_text",
    text: "\n",
  },
};

/**
 * Uses the Section block
 */
export const emptyLineBlock: EmptyLineBlock = {
  type: "section",
  text: {
    type: "plain_text",
    text: "\n",
  },
};
