import { ConfluencePage } from "~/confluence/pages";
import { Block, emptyLineBlock } from "./block-kit";
import moment from "moment";

/**
 * ---- types ----
 */

/**
 * ---- functions ----
 */

/**
 * The Slack message error template.
 *
 * TODO: TS-ify this
 */
export function getPageCreatedBlocks(page: ConfluencePage): Block[] {
  const webuiUrl = `${page._links?.base}${page._links?.webui}`;
  const editUrl = `${page._links?.base}${page._links?.editui}`;
  const tinyUrl = `${page._links?.base}${page._links?.tinyui}`;
  const createdAt = moment(page.createdAt).format("MMMM Do, YYYY"); // TODO: Format this date string

  const blocks = [
    emptyLineBlock,
    {
      type: "section",
      text: {
        type: "mrkdwn",
        text: "A new session page was created.",
      },
    },
    {
      type: "context",
      elements: [
        {
          text: `<${webuiUrl}|View>  <${editUrl}|Edit>  <${tinyUrl}|Tiny>`,
          type: "mrkdwn",
        },
      ],
    },
  ];

  // TODO: Type problem
  return blocks!;
}
