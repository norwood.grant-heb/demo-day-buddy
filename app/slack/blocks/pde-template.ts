// @ts-nocheck

import moment from "moment";

const dividerBlock = {
  type: "divider",
};
const emptyLineBlock = {
  type: "section",
  text: {
    type: "plain_text",
    text: "\n",
  },
};

function getNoData(type: string) {
  return [
    {
      type: "section",
      text: {
        type: "mrkdwn",
        text: `_There seems to be no ${type} this week..._`,
      },
    },
    dividerBlock,
  ];
}

function getBirthdayItem(person) {
  return [
    {
      type: "section",
      text: {
        type: "mrkdwn",
        text: `• *${person.name}*\n_${person.jobTitle}_`,
      },
    },
    {
      type: "context",
      elements: [
        {
          type: "mrkdwn",
          text: `${person.birthday}`,
        },
      ],
    },
    emptyLineBlock,
  ];
}

function getAnniversaryItem(person) {
  return [
    {
      type: "section",
      text: {
        type: "mrkdwn",
        text: `• *${person.name}*\n_${person.jobTitle}_`,
      },
    },
    {
      type: "context",
      elements: [
        {
          type: "mrkdwn",
          text: `${person.anniversary}`,
        },
      ],
    },
    emptyLineBlock,
  ];
}

function getBirthdayItems(birthdayPeople) {
  const birthdays = [];

  if (birthdayPeople.length) {
    birthdayPeople.map(function (person /* , i */) {
      birthdays.push(...getBirthdayItem(person));
    });
    return birthdays;
  } else {
    return getNoData("birthdays");
  }
}

function getAnniversaryItems(anniversaryPeople) {
  const anniversaries = [];

  if (anniversaryPeople.length) {
    anniversaryPeople.map(function (person /* , i */) {
      anniversaries.push(...getAnniversaryItem(person));
    });

    return anniversaries;
  } else {
    console.log("🚨 Something went wrong ...\n" + JSON.stringify(anniversaries, null, 2) + "\n");

    return getNoData("anniversaries");
  }
}

/**
 * Get formatted date text.
 *
 * @returns {string}
 */
function getDateRangeText() {
  const today = moment().startOf("day");
  const endOfThisWeek = today.clone().add(6, "days").endOf("day"); // adding 6 days to account for the full week

  const dateRangeText = `*${today.format("MMMM DD")} - ${endOfThisWeek.format("MMMM DD*")}`; // Format: July 20 – July 26

  return dateRangeText;
}

/**
 * The bdays & anns template used in #pde
 *
 * @param {*} obj
 * @returns
 */
export function getSlackTemplate(obj) {
  const headerBlock = {
    type: "header",
    text: {
      type: "plain_text",
      text: ":heb-digital-gif:  This Week at H-E-B Digital ...",
    },
  };

  const bannerBlock = {
    type: "context",
    elements: [
      {
        type: "mrkdwn",
        text: `– _If data is wrong or missing, ask their manager to update it in Airtable  :themoreyouknow:_`,
      },
    ],
  };

  const weekBlock = {
    type: "context",
    elements: [
      {
        type: "mrkdwn",
        text: getDateRangeText(),
      },
    ],
  };

  const bdayHeadingBlock = {
    type: "section",
    text: {
      type: "mrkdwn",
      text: ":birthday:  *BIRTHDAYS*",
    },
  };

  const anniversaryHeadingBlock = {
    type: "section",
    text: {
      type: "mrkdwn",
      text: ":tada:  *ANNIVERSARIES*",
    },
  };

  const blocks = [];

  const birthdays = getBirthdayItems(obj.birthdayPeople);
  console.log("🥳  birthdays: " + obj.birthdayPeople.length);

  const anniversaries = getAnniversaryItems(obj.anniversaryPeople);
  console.log("🎉  anniversaries: " + obj.anniversaryPeople.length + "\n");

  blocks.push(headerBlock);
  blocks.push(bannerBlock);
  blocks.push(weekBlock);
  blocks.push(emptyLineBlock);
  blocks.push(bdayHeadingBlock);
  blocks.push(dividerBlock);
  blocks.push(...birthdays);
  blocks.push(emptyLineBlock);
  blocks.push(emptyLineBlock);
  blocks.push(anniversaryHeadingBlock);
  blocks.push(dividerBlock);
  blocks.push(...anniversaries);

  const slackTemplate = {
    blocks,
  };

  return slackTemplate;
}
