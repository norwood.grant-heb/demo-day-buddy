import { Block, emptyLineBlock } from "./block-kit";

/**
 * The Slack message error template.
 */
export function getErrorBlocks(message: string): Block[] {
  const blocks = [
    emptyLineBlock,
    {
      type: "header",
      text: {
        type: "plain_text",
        text: ":newspaper:  Paper Company Newsletter  :newspaper:",
      },
    },
    {
      type: "context",
      elements: [
        {
          text: "*November 12, 2019*  |  Sales Team Announcements",
          type: "mrkdwn",
        },
      ],
    },
    {
      type: "divider",
    },
    {
      type: "section",
      text: {
        type: "mrkdwn",
        text: " :loud_sound: *IN CASE YOU MISSED IT* :loud_sound:",
      },
    },
    {
      type: "section",
      text: {
        type: "mrkdwn",
        text: "Replay our screening of *Threat Level Midnight* and pick up a copy of the DVD to give to your customers at the front desk.",
      },
      accessory: {
        type: "button",
        text: {
          type: "plain_text",
          text: "Watch Now",
          emoji: true,
        },
      },
    },
    {
      type: "section",
      text: {
        type: "mrkdwn",
        text: "The *2019 Dundies* happened. \nAwards were given, heroes were recognized. \nCheck out *#dundies-2019* to see who won awards.",
      },
    },
    {
      type: "section",
      text: {
        type: "mrkdwn",
        text: message,
      },
    },
  ];

  return blocks;
}
