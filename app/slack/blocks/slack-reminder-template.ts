import { ConfluencePage } from "~/confluence/pages";
import { Block, emptyLineBlock } from "./block-kit";
import moment from "moment";

/**
 * ---- types ----
 */

/**
 * ---- functions ----
 */

/**
 * The Slack message error template.
 *
 * TODO: TS-ify this
 */
export function getSlackReminderBlocks(
  page: ConfluencePage,
  customTitle: string,
  customMessage: string,
  likeCount: number,
): Block[] {
  const webuiUrl = `${page._links?.base}${page._links?.webui}`;
  const editUrl = `${page._links?.base}${page._links?.editui}`;
  const tinyUrl = `${page._links?.base}${page._links?.tinyui}`;
  const createdAt = moment(page.createdAt).format("MMMM Do, YYYY"); // TODO: Format this date string

  const likesHtml = likeCount >= 0 ? `  |  ${likeCount} likes` : "";
  const customMessageParagraph = customMessage ? customMessage + "\n" : "";

  const blocks = [
    emptyLineBlock,
    {
      type: "header",
      text: {
        type: "plain_text",
        text: customTitle,
      },
    },
    {
      type: "context",
      elements: [
        {
          text: `_<${webuiUrl}|Sign up> in the next 30 mins, and it's free!${likesHtml}_`,
          type: "mrkdwn",
        },
      ],
    },
    {
      type: "divider",
    },
    {
      type: "section",
      text: {
        type: "mrkdwn",
        text: customMessageParagraph,
      },
    },
    {
      type: "section",
      text: {
        type: "mrkdwn",
        text: `So please look at the work you've completed in this last sprint or two and <${webuiUrl}|sign up to show off your work>!`,
      },
    },
    {
      type: "section",
      text: {
        type: "mrkdwn",
        text: `_If:_
    •  it's novel or useful
    •  or it still needs some polish
    •  or has already shipped

*... let's celebrate all the neat stuff we build!*`,
      },
    },
    {
      type: "section",
      text: {
        type: "mrkdwn",
        text: `_Remember, demos are best when you focus on your work, no PowerPoint required._  :think_about_it:\n`,
      },
    },
    emptyLineBlock,
    {
      type: "divider",
    },
    {
      type: "context",
      elements: [
        {
          type: "mrkdwn",
          text: `*Have you thought about creating a video for Demo Day?*  :video_camera:

If you're not comfortable speaking to groups, or you fear the demo gods, or you're otherwise not 100% comfortable with live demos, consider creating a video!

You can link to your video when you <${webuiUrl}|sign up> on the wiki, and we'll play it.`,
        },
      ],
    },
  ];

  // TODO: Type problem
  return blocks!;
}
