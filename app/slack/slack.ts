// import { IncomingWebhook } from "@slack/webhook";
import { handleSlackWebhookError } from "~/utils/errors";

import { ENVIRONMENTS } from "~/constants";
import {
  CURRENT_ENVIRONMENT,
  SLACK_WEBHOOK_URL_PRODUCTION,
  SLACK_WEBHOOK_URL_SANDBOX,
  SLACK_DISABLED_IN_DEVELOPMENT,
} from "~/config/config";

import { slackWebhookPde, slackWebhookSandbox } from "~/slack/constants";
import { getErrorBlocks } from "./blocks/error-template";
import { getPageCreatedBlocks } from "./blocks/page-created-template";
import { IncomingWebhookResult, IncomingWebhookSendArguments } from "@slack/webhook";
import { ConfluencePage } from "~/confluence/pages";
import { Block } from "./blocks/block-kit";
import { logIncomingWebhookResult } from "~/utils/logging";
import { getSlackReminderBlocks } from "./blocks/slack-reminder-template";

// /**
//  * Send the message to the Slack channel using the bdays & anns template used in #pde.
//  *
//  * TODO: Copied over from the other Slack bot app
//  *
//  * @param {*} obj
//  */
// export async function sendMessageWithPDETemplate(obj) {
//   if (SLACK_DISABLED_IN_DEVELOPMENT !== "true") {
//     sendMessage(getSlackTemplate(obj));
//   }
// }

/**
 * Send a Slack message using WebHooks (https://api.slack.com/messaging/webhooks) to the
 * appropriate URL based on CURRENT_ENVIRONMENT.
 *
 * Simple example:
 *    {
 *      text: 'Simple text message',
 *    }
 *
 * See more about the Block Kit (https://api.slack.com/block-kit)
 *
 * Blocks example:
 *    {
 *      blocks: [
 *        {
 *          type: "header",
 *          text: {
 *            type: "plain_text",
 *            text: ":heb-digital-gif:  This Week at H-E-B Digital ...",
 *          },
 *        },
 *        {
 *          type: "context",
 *          elements: [
 *            {
 *              type: "mrkdwn",
 *              text:
 *                `– _An italicized list item_` +
 *                `\n– *A bold list item with emoji*  :themoreyouknow:_`,
 *              },
 *          ],
 *        }
 *      ]
 *    }
 *
 * @param {Object} message
 */
export async function sendMessage(
  message: string | IncomingWebhookSendArguments,
): Promise<IncomingWebhookResult> | unknown {
  let result;
  let webhook;

  try {
    switch (CURRENT_ENVIRONMENT) {
      case ENVIRONMENTS.development:
        // Development env
        console.log(`✅  Sending to webhook "${SLACK_WEBHOOK_URL_SANDBOX}" (development) ...`);

        // Send to the sandbox webhook.
        webhook = slackWebhookSandbox;
        result = await webhook.send(message);

        // TODO: Check for warnings (https://api.slack.com/changelog/2018-04-truncating-really-long-messages)

        console.log("✅  Slack Message Sent to #pde channel!\n");

        break;

      case ENVIRONMENTS.production:
        // Production env
        console.log(`✅  Sending to webhook "${SLACK_WEBHOOK_URL_PRODUCTION}" (production) ...`);

        // Send to the PDE webhook.
        webhook = slackWebhookPde;
        result = await webhook.send(message);

        // TODO: Check for warnings (https://api.slack.com/changelog/2018-04-truncating-really-long-messages)

        console.log("✅  Slack Message Sent to #pde channel!\n");

        break;
    }
  } catch (e) {
    handleSlackWebhookError(e, true);

    return e;
  }

  logIncomingWebhookResult(result!);

  return result;
}

/**
 * Send an error message to Slack.
 *
 * TODO:  Send directly to Grant?  Or to the Sandbox channel
 */
export async function sendErrorMessage(message: string): Promise<IncomingWebhookResult> {
  let result: IncomingWebhookResult;

  const blocks = getErrorBlocks(message);

  try {
    // Send to the sandbox webhook.

    result = await slackWebhookSandbox.send({ blocks });

    logIncomingWebhookResult(result);
  } catch (e) {
    // TODO: handleError(e);
    return e;
  }

  return result;
}

/**
 * Send a message to Slack when a new session page is created.
 */
export async function sendMessage_newPageCreated(page: ConfluencePage): Promise<IncomingWebhookResult> {
  let result: IncomingWebhookResult;

  const blocks = getPageCreatedBlocks(page);

  try {
    // Send to the sandbox webhook.
    // TODO: Make a util function for isDevelopment()
    if (SLACK_DISABLED_IN_DEVELOPMENT !== "true") {
      result = await slackWebhookSandbox.send({ blocks });
    } else {
      result = await slackWebhookPde.send({ blocks });
    }

    logIncomingWebhookResult(result);
  } catch (e) {
    // TODO: handleError(e);
    return e;
  }

  return result;
}

/**
 * Send a reminder message to Slack.
 */
export async function sendMessage_slackReminder(
  page: ConfluencePage,
  customTitle: string,
  customMessage: string,
  likeCount: number,
): Promise<IncomingWebhookResult> {
  let result: IncomingWebhookResult;

  const blocks = getSlackReminderBlocks(page, customTitle, customMessage, likeCount);

  try {
    // Send to the sandbox webhook.
    // TODO: Make a util function for isDevelopment()
    if (SLACK_DISABLED_IN_DEVELOPMENT !== "true") {
      result = await slackWebhookSandbox.send({ blocks });
    } else {
      result = await slackWebhookPde.send({ blocks });
    }

    logIncomingWebhookResult(result);
  } catch (e) {
    // TODO: handleError(e);
    return e;
  }

  return result;
}
