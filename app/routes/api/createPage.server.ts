import type { ActionFunctionArgs } from "@remix-run/node";

import { json } from "@remix-run/node";

import { CONFLUENCE_SPACE_ID } from "~/config/config";
import { logApiErrors, logAppErrors, logErrorMessage, logIncomingWebhookResult, logInfo } from "~/utils/logging";
import {
  ConfluencePage,
  CreateNewSessionPageRequestArgs,
  CreatePageRequestBody,
  Track,
  createPage,
} from "~/confluence/pages";
import { getPageProperties, findPagePropertyByKey, updatePageProperty } from "~/confluence/pageProperties";
import { sendMessage_newPageCreated } from "~/slack/slack";
import { AppError } from "~/utils/errors";
// import { handleError } from "~/utils/errors";

/**
 * The Remix action handler for this route.
 */
export async function action({ request }: ActionFunctionArgs) {
  if (request.method !== "POST") {
    // TODO: Use a base error object
    return json({ errors: [{ status: 400, title: "Request must use POST method" }] });
  }

  const req = await request.json();
  const date = await req.date;
  const customMessage = await req.customMessage;
  const customTitle = await req.customTitle;
  const featuredTracks = await req.featuredTracks;

  // Verify date is non-null
  // TODO: ... and the correct short date string, e.g. "2024-05-15"
  if (!date) {
    return json({ errors: [{ status: 400, title: "Missing `date` from request body" }] });
  }

  // Verify message is non-null ??  maybe just for dev
  if (!customMessage) {
    return json({ errors: [{ status: 400, title: "Missing `customMessage` from request body" }] });
  }

  // Verify customTitle is non-null ??  maybe just for dev
  if (!customTitle) {
    return json({ errors: [{ status: 400, title: "Missing `customTitle` from request body" }] });
  }

  // Verify featuredTracks is non-null ??  maybe just for dev
  if (!featuredTracks) {
    return json({ errors: [{ status: 400, title: "Missing `featuredTracks` from request body" }] });
  }

  const result = await createNewSessionPage({ date, customTitle, customMessage, featuredTracks });

  // The api returns an `{ errors: [] }` object when errors occur.
  if (result.errors) {
    logApiErrors(result.errors);
  }

  return json(result);
}

export function getFeaturedTracksListItems(tracks: Track[]): string {
  if (!tracks) {
    return "<li>_No featured tracks yet ...</li>";
  }
  const listItems: string[] = [];
  tracks.forEach((track) => {
    listItems.push(`<a href="${track.url}">${track.title}</a>`);
  });

  return listItems.join("");
}

/**
 * Create a new page in Confluence, and set some initial properties.
 *
 * @param {string}      date              The short date string, e.g. "2024-05-15"
 * @param {string}      customMessage     A custom message displayed in the Slack post template.
 * @returns
 */
export async function createNewSessionPage(args: CreateNewSessionPageRequestArgs) {
  // TODO: `args[date]` format should be validated before inserting into the title

  const featuredTracksListItems = getFeaturedTracksListItems(args.featuredTracks);

  // DEBUG
  logInfo(featuredTracksListItems);

  const body: CreatePageRequestBody = {
    spaceId: CONFLUENCE_SPACE_ID,
    status: "current",
    title: `${args.date} - Cross Squad Demo Day`,
    parentId: "3625059780",
    body: {
      representation: "storage",
      value: `<ac:adf-extension><ac:adf-node type="panel"><ac:adf-attribute key="panel-type">note</ac:adf-attribute><ac:adf-content><p>Please sign up for anything you&rsquo;d like to show off, or to simply get feedback on work-in-progress.</p><p><strong><span style="color: rgb(101,84,192);">If it&rsquo;s novel or useful, if it still needs some polish or has already shipped, let&rsquo;s celebrate all the neat stuff we build!</span></strong>  🍾</p></ac:adf-content></ac:adf-node><ac:adf-fallback><div class="panel conf-macro output-block" style="background-color: rgb(234,230,255);border-color: rgb(153,141,217);border-width: 1.0px;"><div class="panelContent" style="background-color: rgb(234,230,255);">\n<p>Please sign up for anything you&rsquo;d like to show off, or to simply get feedback on work-in-progress.</p><p><strong><span style="color: rgb(101,84,192);">If it&rsquo;s novel or useful, if it still needs some polish or has already shipped, let&rsquo;s celebrate all the neat stuff we build!</span></strong>  🍾</p>\n</div></div></ac:adf-fallback></ac:adf-extension><ac:structured-macro ac:name="panel" ac:schema-version="1" ac:macro-id="33948c75-92e9-491f-86b4-76ce1d4f8093"><ac:parameter ac:name="panelIcon">:movie_camera:</ac:parameter><ac:parameter ac:name="panelIconId">1f3a5</ac:parameter><ac:parameter ac:name="panelIconText">🎥</ac:parameter><ac:parameter ac:name="bgColor">#E3FCEF</ac:parameter><ac:rich-text-body><p>Recordings can be found in <a href="https://partnernetheb.sharepoint.com/:f:/s/CXEngineering/Ekbztdog861LigREo8ZLZ0QBqT8RI85B10yK_ReHKhSnzQ?e=bmNeGB">this OneDrive folder</a></p></ac:rich-text-body></ac:structured-macro><ac:structured-macro ac:name="panel" ac:schema-version="1" ac:macro-id="101dc643-6179-4460-a02a-7d59ae03fcc5"><ac:parameter ac:name="panelIcon">:notes:</ac:parameter><ac:parameter ac:name="panelIconId">1f3b6</ac:parameter><ac:parameter ac:name="panelIconText">🎶</ac:parameter><ac:parameter ac:name="bgColor">#DEEBFF</ac:parameter><ac:rich-text-body><p><strong>Featured tracks!</strong></p><ul>${featuredTracksListItems}</ul></ac:rich-text-body></ac:structured-macro><table data-table-width="960" data-layout="wide" ac:local-id="007a9fde-3294-488b-88fa-e2db5f7da485"><colgroup><col style="width: 226.0px;" /><col style="width: 160.0px;" /><col style="width: 309.0px;" /><col style="width: 265.0px;" /></colgroup><tbody><tr><th><p>Presenter</p></th><th><p>Squad/Vertical</p></th><th><p>Feature to Share</p></th><th><p>Optional Notes &amp; Links</p></th></tr><tr><td data-highlight-colour="#ffebe6" colspan="4"><p style="text-align: center;">🚨 <ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="34f1c185-8e29-4a1a-9cb6-2910cbaaecc1"><ac:parameter ac:name="title">press record!</ac:parameter><ac:parameter ac:name="colour">Red</ac:parameter></ac:structured-macro></p></td></tr><tr><td><p /></td><td><p /></td><td><p /></td><td><p /></td></tr><tr><td><p /></td><td><p /></td><td><p /></td><td><p /></td></tr><tr><td><p /></td><td><p /></td><td><p /></td><td><p /></td></tr><tr><td><p /></td><td><p /></td><td><p /></td><td><p /></td></tr><tr><td><p /></td><td><p /></td><td><p /></td><td><p /></td></tr><tr><td><p /></td><td><p /></td><td><p /></td><td><p /></td></tr><tr><td><p /></td><td><p /></td><td><p /></td><td><p /></td></tr><tr><td><p /></td><td><p /></td><td><p /></td><td><p /></td></tr><tr><td data-highlight-colour="#b3d4ff" colspan="4"><p style="text-align: center;"><ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="8b2bdcf8-8a04-40f6-b6e8-8b8cdb4f702e"><ac:parameter ac:name="title">Final Q&amp;A?</ac:parameter><ac:parameter ac:name="colour">Blue</ac:parameter></ac:structured-macro> </p></td></tr><tr><td data-highlight-colour="#b3d4ff" colspan="4"><p style="text-align: center;"><ac:structured-macro ac:name="status" ac:schema-version="1" ac:macro-id="9857e109-2ee1-4d76-b554-6e8be62ec517"><ac:parameter ac:name="title">other announcements?</ac:parameter><ac:parameter ac:name="colour">Blue</ac:parameter></ac:structured-macro></p></td></tr></tbody></table><p><br /></p><p />`,
    },
  };

  // DEBUG
  // logInfo(`About to create a new session page for "${date}" called "${data.title}" ...`);

  const createPageResult = await createPage(args.date, body);

  // Check for errors.
  if (createPageResult.errors) {
    logApiErrors(createPageResult.errors);

    return createPageResult;
  }

  const newPage = createPageResult as ConfluencePage;

  // Verify we got a new page id
  if (!newPage.id) {
    // TODO: Why is this being called when the page was created successfully? 🤷‍♂️
    logErrorMessage("Could not create the new session page");
    logApiErrors(createPageResult.errors);

    // Return before we try to update page properties below
    return createPageResult;
  }

  updateInitialSessionPageProperties(newPage.id);

  // Send a Slack message with the new page info.
  const sendResult = await sendMessage_newPageCreated(newPage);

  logIncomingWebhookResult(sendResult);

  // TODO: Handle errors from sendResult()

  return createPageResult;
}

/**
 * Sets some initial properties on the page.
 */
export async function updateInitialSessionPageProperties(pageId: string) {
  // Update the initial properties for the page
  const properties = await getPageProperties({ pageId });

  const publishedWidthProperty = findPagePropertyByKey("content-appearance-published", properties);
  const draftWidthProperty = findPagePropertyByKey("content-appearance-draft", properties);

  if (!publishedWidthProperty) {
    const errors: AppError[] = [{ message: "Could not update `publishedWidthProperty` page property" }];
    logAppErrors(errors);
    return { errors };
  }

  if (!draftWidthProperty) {
    const errors: AppError[] = [{ message: "Could not update `draftWidthProperty` page property" }];
    logAppErrors(errors);
    return { errors };
  }

  // DEBUG
  logInfo(`publishedWidthProperty: ${JSON.stringify(publishedWidthProperty, null, 2)}`);
  logInfo(`draftWidthProperty: ${JSON.stringify(draftWidthProperty, null, 2)}`);

  // TODO: Handle errors 👇

  // Be sure to increment the version number!
  const publishedPropertyResult = await updatePageProperty({
    pageId,
    propertyId: publishedWidthProperty.id,
    key: publishedWidthProperty.key,
    value: "default", // updated value
    versionNumber: publishedWidthProperty.version.number + 1,
  });

  if (publishedPropertyResult.errors) {
    logInfo(`publishedPropertyResult (error):\n${JSON.stringify(publishedPropertyResult, null, 2)}`);
  } else {
    logInfo(`publishedPropertyResult\n${JSON.stringify(publishedPropertyResult, null, 2)}`);
  }

  // Be sure to increment the version number!
  const draftPropertyResult = await updatePageProperty({
    pageId,
    propertyId: draftWidthProperty.id,
    key: draftWidthProperty.key,
    value: "default", // updated value
    versionNumber: draftWidthProperty.version.number + 1,
  });

  if (draftPropertyResult.errors) {
    logApiErrors(draftPropertyResult.errors);
  } else {
    logInfo(`draftPropertyResult\n${JSON.stringify(draftPropertyResult, null, 2)}`);
  }

  return { success: !publishedPropertyResult.errors && !draftPropertyResult.errors };
}
