import type { ActionFunctionArgs } from "@remix-run/node";

import { json } from "@remix-run/node";

import { logApiErrors, logAppErrors, logErrorMessage, logIncomingWebhookResult, logInfo } from "~/utils/logging";
import { ConfluencePage, getPageById, getPageLikeCount } from "~/confluence/pages";
import { getPageProperties, findPagePropertyByKey, updatePageProperty } from "~/confluence/pageProperties";
import { sendMessage_slackReminder } from "~/slack/slack";
import { AppError } from "~/utils/errors";
// import { handleError } from "~/utils/errors";

/**
 * The Remix action handler for this route.
 */
export async function action({ request }: ActionFunctionArgs) {
  if (request.method !== "POST") {
    // TODO: Use a base error object
    return json({ errors: [{ status: 400, title: "Request must use POST method" }] });
  }

  const req = await request.json();
  const pageId = await req.pageId;
  const customMessage = await req.customMessage;
  const customTitle = await req.customTitle;

  // Verify pageId is non-null
  if (!pageId) {
    return json({ errors: [{ status: 400, title: "Missing `pageId` from request body" }] });
  }

  // Verify message is non-null
  if (!customMessage) {
    return json({ errors: [{ status: 400, title: "Missing `customMessage` from request body" }] });
  }

  // Verify customTitle is non-null
  if (!customTitle) {
    return json({ errors: [{ status: 400, title: "Missing `customTitle` from request body" }] });
  }

  const result = await sendSlackReminder(pageId, customTitle, customMessage);

  // The api returns an `{ errors: [] }` object when errors occur.
  if (result.errors) {
    logApiErrors(result.errors);
  }

  return json(result);
}

/**
 * Create a new page in Confluence, and set some initial properties.
 */
export async function sendSlackReminder(pageId: string, customTitle: string, customMessage: string) {
  // Get the page by id
  const page: ConfluencePage = await getPageById(pageId);

  // Check for errors.
  if (page.errors) {
    logApiErrors(page.errors);

    return page;
  }

  // Verify we got a new page id
  if (!page.id) {
    // TODO: Why is this being called when the page was created successfully? 🤷‍♂️
    logErrorMessage("Could not create the new session page");
    logApiErrors(page.errors);

    // Return before we try to update page properties below
    return page;
  }

  const likeCount: number = await getPageLikeCount(pageId);

  // Send a Slack message with the new page info.
  const sendResult = await sendMessage_slackReminder(page, customTitle, customMessage, likeCount);

  logIncomingWebhookResult(sendResult);

  // TODO: Handle errors from sendResult()

  return page;
}

/**
 * Sets some initial properties on the page.
 */
export async function updateInitialSessionPageProperties(pageId: string) {
  // Update the initial properties for the page
  const properties = await getPageProperties({ pageId });

  const publishedWidthProperty = findPagePropertyByKey("content-appearance-published", properties);
  const draftWidthProperty = findPagePropertyByKey("content-appearance-draft", properties);

  if (!publishedWidthProperty) {
    const errors: AppError[] = [{ message: "Could not update `publishedWidthProperty` page property" }];
    logAppErrors(errors);
    return { errors };
  }

  if (!draftWidthProperty) {
    const errors: AppError[] = [{ message: "Could not update `draftWidthProperty` page property" }];
    logAppErrors(errors);
    return { errors };
  }

  // DEBUG
  logInfo(`publishedWidthProperty: ${JSON.stringify(publishedWidthProperty, null, 2)}`);
  logInfo(`draftWidthProperty: ${JSON.stringify(draftWidthProperty, null, 2)}`);

  // TODO: Handle errors 👇

  // Be sure to increment the version number!
  const publishedPropertyResult = await updatePageProperty({
    pageId,
    propertyId: publishedWidthProperty.id,
    key: publishedWidthProperty.key,
    value: "default", // updated value
    versionNumber: publishedWidthProperty.version.number + 1,
  });

  if (publishedPropertyResult.errors) {
    logInfo(`publishedPropertyResult (error):\n${JSON.stringify(publishedPropertyResult, null, 2)}`);
  } else {
    logInfo(`publishedPropertyResult\n${JSON.stringify(publishedPropertyResult, null, 2)}`);
  }

  // Be sure to increment the version number!
  const draftPropertyResult = await updatePageProperty({
    pageId,
    propertyId: draftWidthProperty.id,
    key: draftWidthProperty.key,
    value: "default", // updated value
    versionNumber: draftWidthProperty.version.number + 1,
  });

  if (draftPropertyResult.errors) {
    logApiErrors(draftPropertyResult.errors);
  } else {
    logInfo(`draftPropertyResult\n${JSON.stringify(draftPropertyResult, null, 2)}`);
  }

  return { success: !publishedPropertyResult.errors && !draftPropertyResult.errors };
}
