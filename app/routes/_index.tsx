import type { MetaFunction } from "@remix-run/node";

export const meta: MetaFunction = () => {
  return [{ title: "Demo Day Buddy" }, { name: "description", content: "Welcome to CX Demo Day!" }];
};

export default function Index() {
  return (
    <div style={{ fontFamily: "system-ui, sans-serif", lineHeight: "1.8" }}>
      <h1>Demo Day Buddy</h1>
    </div>
  );
}
