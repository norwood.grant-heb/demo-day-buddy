import { vitePlugin as remix } from "@remix-run/dev";
import { installGlobals } from "@remix-run/node";
import { defineConfig } from "vite";
import tsconfigPaths from "vite-tsconfig-paths";

installGlobals();

export default defineConfig({
  plugins: [
    remix({
      routes(defineRoutes) {
        return defineRoutes((route) => {
          // route("/", "home/route.tsx", { index: true });
          // route("about", "about/route.tsx");
          // route("concerts", "concerts/layout.tsx", () => {
          //   route("", "concerts/home.tsx", { index: true });
          //   route("trending", "concerts/trending.tsx");
          //   route(":city", "concerts/city.tsx");
          // });

          route("/api/sendSlackReminder", "routes/api/sendSlackReminder.server.ts");
          route("/api/createPage", "routes/api/createPage.server.ts");
        });
      },
    }),
    tsconfigPaths(),
  ],
});
